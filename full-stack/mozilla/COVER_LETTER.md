Jeremy Seago
============

2418 Florent Ave. | St. Louis, MO 63143 | [314-467-0643](tel:314-467-0643) | [seagoj@gmail.com](mailto:seagoj@gmail.com)

__December 18, 2021__

Dear Hiring Manager,

With over 15 years experience in fullstack software development and systems architecture, I am confident that I'm an excellent fit for the Fullstack Engineer at Mozilla. My experience has honed the skills necessary to provide innovative solutions to a wide array of problems concerning a highly utilized API and web client. My focus on performance, security and testing will allow Mozilla to grow without fear of regression. My familiarity with a startup evironment means I'm well adapted to rapid prototyping and changing landscapes so I can deliver on projects that morph over time.

In my current position I'm responsible for leading development on a high-traffic communications API. I plan, design and break new work down into ticketable issues to maintain velocity and help my team deliver. We accomplish this with an architecture of VueJs, Node, Javascript, PHP, MySQL, Redis, Python & Elasticsearch. My extensive knowledge of both REST and GraphQL APIs and strong Javascript architecture has been instrumental in design and implementation of many projects including, most recently an integration with Microsoft Teams and a rewrite of our notification queue. I also collaborate well with all areas of the company to surpass expectations and deliver the best possible product.

I would very much enjoy learning more about Mozilla's specific needs and how I may be a fit. Working for a company that has personally been such a large part of my life in computing (I have been a user since the Firebird days with the cool Shepard Fairey logo.) that has such a strong focus on maintaining an open web has always been a dream job of mine. I'd love the opportunity to demonstrate how I'd be an asset to the team.

Thanks for your consideration,

Jeremy Seago
