Jeremy Seago
============

2418 Florent Ave. | St. Louis, MO 63143 | [314-467-0643](tel:314-467-0643) | [seagoj@gmail.com](mailto:seagoj@gmail.com)

__December 18, 2021__

Dear Hiring Manager,

With over 15 years experience in backend software development and systems architecture, I am confident that I'm an excellent fit for the Senior Backend Engineer at GitLab. My experience has honed the skills necessary to provide innovative solutions to a wide array of problems concerning a highly utilized API. My focus on performance, security and testing will allow GitLab to grow without fear of regression.

In my current position I'm responsible for leading development on a high-traffic communications API. I plan, design and break new work down into ticketable issues to maintain velocity and help my team deliver. We accomplish this with an architecture of PHP, MySQL, Redis, Python, Node & Elasticsearch. My extensive knowledge of both REST and GraphQL APIs has been instrumental in design and implementation of many projects including, most recently an integration with Microsoft Teams and a rewrite of our notification queue. I also collaborate well with all areas of the company to surpass expectations and deliver the best possible product.

I would very much enjoy learning more about GitLab's specific needs and how I may be a fit. Working for a company that is such a huge part of maintaining, producing and promoting open source has always been a dream job of mine, especially a company that I use extensively myself. I'd love the opportunity to demonstrate how I'd be an asset to the team.

Thanks for your consideration,

Jeremy Seago
