\pagenumbering{gobble}

Jeremy Seago
============

2418 Florent Ave. | St. Louis, MO 63143 | [314-467-0643](tel:314-467-0643) | [seagoj@gmail.com](mailto:seagoj@gmail.com)    


- linkedin: [https://www.linkedin.com/in/seagoj](https://www.linkedin.com/in/seagoj)
- gitlab: [https://gitlab.com/seagoj](https://gitlab.com/seagoj)
- github: [https://github.com/seagoj](https://github.com/seagoj)    

## Qualification Summary

__Senior Backend Engineer__ with extensive experience in REST API development and design, relational databases, caching layers, security and performance tuning as well as modern Javascript frameworks. I am well versed in agile programming practices and SCRUM methodologies. I have used these skills to collect requirements, plan design and deliver on varied projects covering a wide range of use cases.

## Technical Skills

- __Languages__: PHP, Python, Node, Rust, Go, Java, C/C++, C#
- __Datastores__: MySQL, Redis, PostgreSQL, GraphQL, MSSQL

## Relevent Professional Experience

### Senior Full Stack Engineer

Bonfyre, St. Louis, MO    
March 2016 - November 2018, July 2019 - present    

- Design and develop secure and highly available API, caching and data layer consumed by web and mobile clients
- Owned and implemented features at all levels of the stack
- Implemented UI/UX designs in VueJS with consideration for security and fast response
- Planned and executed integration with Microsoft Teams using AWS Lambda functions
- Implemented pagination and fuzzy searching capabilities across encrypted user data

### Project Lead Developer

Cordant Health Solutions, Denver, CO    
November 2018 - July 2019    

- Lead Full Stack Engineer on project to replace a highly leveraged legacy system using modern technologies
- Planned the work and assigned tickets to the other members of the team
- Provided education and mentorship to the team
- Replaced legacy PERL and MySQL API with Node and GraphQL
- Replaced legacy frontend with React/Redux and modified Material Design components

### Software Developer

Bellevue Pharmacy/Fagron North America, St. Louis, MO   
October 2013 - March 2016   

- Development of a data entry and document/process management application for an international pharmacy
- Sole developer on project turning stakeholder requirements into workable tickets
- Designed as a responsive React based frontend and a PHP REST API which exposes the relational database and file store for document annotation and manipulation
- Managed deployment for development, staging and production environments using Git
- Deployments tested automatically via (self maintained) Jenkins prior to getting pushed to the server
- Responsible for all DevOps across the development, staging and production environments
- Staging and Production virtual servers provisioned with Ansible
- Development done on local vagrant virtual machine
- Developed frontend using Flexbox, React and Redux

## Education

Illinois College    
Jacksonville, IL    

- Bachelor of Science in Computer Science
- Mathematics minor

___References are available upon request___
